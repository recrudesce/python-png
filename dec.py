#! /usr/bin/env python

import sys
import binascii
from PIL import Image
import hashlib
import subprocess
from Crypto.Cipher import ARC4


def HTMLColorToRGB(colorstring):
    """ convert #RRGGBB to an (R, G, B) tuple """
    colorstring = colorstring.strip()
    if colorstring[0] == '#':
        colorstring = colorstring[1:]
    if len(colorstring) != 6:
        raise ValueError, "input #%s is not in #RRGGBB format" % colorstring
    r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return (r, g, b)


def RGBToHTMLColor(rgb_tuple):
    """ convert an (R, G, B) tuple to #RRGGBB """
    hexcolor = '%02x%02x%02x' % rgb_tuple
    # that's it! '%02x' means zero-padded, 2-digit hex values
    return hexcolor

filename = sys.argv[1]
hex_ = ""
inData = []
im = Image.open(filename)
inData = im.getdata()
for ind in inData:
    hex_ = hex_ + RGBToHTMLColor(ind)

padding = "123456"
padding_location = hex_.rfind(padding)

orig_file_len = len(hex_) - (len(hex_)-padding_location)
orig_file = hex_[0:orig_file_len]

for p in range(10, 6, -2):
    check_p = hex_[orig_file_len:orig_file_len + p]
    print check_p
    if "1234567890" in check_p:
        q = 10
        key_ = hex_[orig_file_len + q:orig_file_len + q + 32]
        break
    elif "12345678" in check_p:
        q = 8
        key_ = hex_[orig_file_len + q:orig_file_len + q + 32]
        break
    elif "123456" in check_p:
        q = 6
        key_ = hex_[orig_file_len + q:orig_file_len + q + 32]
        break
    else:
        print "key could not be found"

if ".bmp" in filename:
    command = binascii.unhexlify(orig_file)
    cmd_results = subprocess.check_output(command.split(), stderr=subprocess.STDOUT)
    content = command + "\n\n" + cmd_results
    m = hashlib.md5()
    m.update(content)
    filename_out = m.hexdigest()
    print filename_out
    ext = ".png"
    enc = "PNG"

    hex_ = binascii.hexlify(content)
    hex_pad = len(hex_) % 6
    padding = "1234567890"

    if hex_pad > 0:
            padding_amount = 6-hex_pad
            hex_ = hex_ + padding[:padding_amount]
    else:
            padding_amount = 0

    hex_ = hex_ + padding[padding_amount:padding_amount+6]

    background = (0, 0, 0)
    im2 = Image.new('RGB', (400, (len(hex_)/6/400+1)), color=background)

    newData = []

    for i in range(0, len(hex_), 6):
            chunk = hex_[i:i+6]
            chunk_pad = len(chunk) % 6
            chunk_rgb = HTMLColorToRGB(chunk)
            newData.append(chunk_rgb)

    im2.putdata(newData)
    im2.save(filename[:-4] + "_results" + ext, enc, compress_level=0)

    print "Command results encoded to " + filename[:-4] + "_results" + ext

if ".png" in filename:
    random_ = key_
    random__ = random_[:16]
    random___ = random_[16:]
    random____ = random___[::-1]

    xor = '%x' % (int(random__, 16)^int(random____, 16))

    md5 = hashlib.md5(xor).hexdigest()
    obj2 = ARC4.new(md5)

    orig_file_dec = binascii.hexlify(obj2.decrypt(binascii.unhexlify(orig_file)))

    orig_file_out = open(filename + ".dec", "wb")
    orig_file_out.write(bytearray(binascii.unhexlify(orig_file_dec)))
    orig_file_out.close()
print "Done !"
