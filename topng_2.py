#! /usr/bin/env python
#test
import sys
import binascii
from PIL import Image
import uuid
import hashlib
import random

from Crypto.Cipher import ARC4


def HTMLColorToRGB(colorstring):
    """ convert #RRGGBB to an (R, G, B) tuple """
    colorstring = colorstring.strip()
    if colorstring[0] == '#':
        colorstring = colorstring[1:]
    if len(colorstring) != 6:
        raise ValueError, "input #%s is not in #RRGGBB format" % colorstring
    r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
    r, g, b = [int(n, 16) for n in (r, g, b)]
    return (r, g, b)

type = sys.argv[1]
if type == "cmd":
    content = sys.argv[2]
    m = hashlib.md5()
    m.update(content)
    filename_out = m.hexdigest()

    ext = ".bmp"
    enc = "BMP"
if type == "file":
    filename = sys.argv[2]
    with open(filename, 'rb') as f:
        content = f.read()
        ext = ".png"
        enc = "PNG"
    m = hashlib.md5()
    m.update(content)
    filename_out = m.hexdigest()

random_ = uuid.uuid4().hex
random__ = random_[:16]
random___ = random_[16:]
random____ = random___[::-1]

print "Random generated uuid: " + random_
print "First 16: " + random__
print "Last 16 reversed: " + random____

xor = '%x' % (int(random__, 16) ^ int(random____, 16))

print "XOR'd: " + xor

md5 = hashlib.md5(xor).hexdigest()

print "MD5 of XOR: " + md5

rcfore = ARC4.new(md5)
cipher_text = rcfore.encrypt(content)
hex_ = binascii.hexlify(cipher_text)
hex_pad = len(hex_ + random_) % 6
padding = "1234567890"

if hex_pad > 0:
    if hex_pad == 4:
        padding_amount = 2
        hex_ = hex_ + padding[:padding_amount]
        hex_ = hex_ + padding[padding_amount:padding_amount+6] + random_
    else:  
        padding_amount = 6-hex_pad
        hex_ = hex_ + padding[:padding_amount]
        hex_ = hex_ + padding[padding_amount:padding_amount+6] + random_
else:
    padding_amount = 0
    hex_ = hex_ + padding[padding_amount:padding_amount+6] + random_
print "Padding: " + padding[:padding_amount]

rubbish_pad = len(hex_) % 2400
if rubbish_pad > 0:
    amount_to_pad = 2400 - rubbish_pad
    random_rubbish = ""
    while (len(random_rubbish) < amount_to_pad) is True:
        random_rubbish = random_rubbish + uuid.uuid4().hex
    random_rubbish_pad = random_rubbish[:amount_to_pad-6]

    print "Random Pad: " + random_rubbish_pad + " length " + str(len(random_rubbish_pad))
    hex_ = hex_ + random_rubbish_pad

random.seed()
bkgnd1 = random.randrange(0, 255, 1)
bkgnd2 = random.randrange(0, 255, 1)
bkgnd3 = random.randrange(0, 255, 1)

background = (bkgnd1, bkgnd2, bkgnd3)
im2 = Image.new('RGB', (400, (len(hex_)/6/400+1)), color=background)

newData = []

for i in range(0, len(hex_), 6):
        chunk = hex_[i:i+6]
        chunk_pad = len(chunk) % 6
        chunk_rgb = HTMLColorToRGB(chunk)
        newData.append(chunk_rgb)

im2.putdata(newData)

if type == "file":
    im2.save(filename_out + ext, enc, compress_level=0)
else:
    im2.save(filename_out + ext, enc)

print "encoded to " + filename_out + ext
